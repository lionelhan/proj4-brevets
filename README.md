### Project 4: Brevet time calculator with Ajax

## author: Qi Han     email: qhan@uoregon.edu


## Project description:

1. Create a replacement of brevet control time calculator
2. The distance is 200, 300, 400, 600 and 1000
3. Revise the code in flask_brevets and make the application working
4. Modify the acp_times file to calculate the minimum ad maximum time by which the rider muct arrive at the location
5. Create a nose test to test the app
6. Upload the credential file on canvas, push the README, dockerfile, flask_brevets, calc, acp_times file back to the bitbucket

