"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from dateutil import tz

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments. 
#


def open_time( control_dist_km, brevet_dist_km, brevet_start_time  ):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet in kilometers,
           which must be one of 200, 300, 400, 600, or 1000 (the only official
           ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    time_used = 0.0
    the_TIME = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    the_km = [(600,28.0),(400,30.0),(200,32.0),(0,34.0)]
    if brevet_dist_km > control_dist_km:
        brevet_dist_km = control_dist_km
    
    for i in range(len(the_km)):
        if brevet_dist_km > the_km[i][0]:
            dif = brevet_dist_km - the_km[i][0]
            time = round(dif/float(the_km[i][1]),3)
            time_used += time
            brevet_dist_km -= dif
        min = round(time_used * 60,0)
    final_time = the_TIME.replace(minutes=+min).isoformat()
   

    return final_time

def close_time( control_dist_km, brevet_dist_km, brevet_start_time ):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet in kilometers,
           which must be one of 200, 300, 400, 600, or 1000 (the only official
           ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    time_used = 0.0
    min = 0
    the_TIME = arrow.get(brevet_start_time, 'YYYY-MM-DD HH:mm')
    the_km = [(600,11.428),(400,15.0),(200,15.0),(0,15.0)]
#for the special for 200 km:
    if brevet_dist_km == 200:
        if control_dist_km >= 200:
            min_for_200 = 810
            final_time = the_TIME.replace(minutes=+min_for_200).isoformat()
            return final_time

#for the speical for 400 km:
    if brevet_dist_km == 400:
        if control_dist_km >= 400:
            min = min + 20

    if brevet_dist_km > control_dist_km:
        brevet_dist_km = control_dist_km

    for i in range(len(the_km)):
        if brevet_dist_km > the_km[i][0]:
            dif = brevet_dist_km - the_km[i][0]
            time = (dif/float(the_km[i][1]))
            time_used += time
            brevet_dist_km -= dif
    min = min + round(time_used*60,0)


#for the special for control_dist = 0:
    if min == 0:
        min = 60
    
    final_time = the_TIME.replace(minutes=+min).isoformat()
    return final_time
