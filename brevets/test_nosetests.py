import arrow
from acp_times import open_time
from acp_times import close_time


def test_general_200():
    DIST = 200
    START = "2000-01-01 10:10"
    CASES = [[0,arrow.get("2000-01-01 10:10"), arrow.get("2000-01-01 11:10")],
             [50,arrow.get("2000-01-01 11:38"), arrow.get("2000-01-01 13:30")],
             [80,arrow.get("2000-01-01 12:31"), arrow.get("2000-01-01 15:30")],
             [130,arrow.get("2000-01-01 13:59"), arrow.get("2000-01-01 18:50")],
             [200,arrow.get("2000-01-01 16:03"), arrow.get("2000-01-01 23:40")],
             ]
    for case in CASES:
        assert open_time(case[0], DIST, START) == case[1].isoformat()
        assert close_time(case[0], DIST, START) == case[2].isoformat()


def test_general_400():
    DIST = 400
    START = "2000-01-01 10:10"
    CASES = [[0,arrow.get("2000-01-01 10:10"), arrow.get("2000-01-01 11:10")],
             [100,arrow.get("2000-01-01 13:06"), arrow.get("2000-01-01 16:50")],
             [200,arrow.get("2000-01-01 16:03"), arrow.get("2000-01-01 23:30")],
             [300,arrow.get("2000-01-01 19:10"), arrow.get("2000-01-02 06:10")],
             [400,arrow.get("2000-01-01 22:18"), arrow.get("2000-01-02 13:10")],
             ]
    for case in CASES:
        assert open_time(case[0], DIST, START) == case[1].isoformat()
        assert close_time(case[0], DIST, START) == case[2].isoformat()


def test_general_300():
    DIST = 300
    START = "2000-01-01 10:10"
    CASES = [[0,arrow.get("2000-01-01 10:10"), arrow.get("2000-01-01 11:10")],
             [100,arrow.get("2000-01-01 13:06"), arrow.get("2000-01-01 16:50")],
             [200,arrow.get("2000-01-01 16:03"), arrow.get("2000-01-01 23:30")],
             [250,arrow.get("2000-01-01 17:37"), arrow.get("2000-01-02 02:50")],
             [300,arrow.get("2000-01-01 19:10"), arrow.get("2000-01-02 06:10")],
             ]
    for case in CASES:
        assert open_time(case[0], DIST, START) == case[1].isoformat()
        assert close_time(case[0], DIST, START) == case[2].isoformat()



def test_general_1000():
    DIST = 1000
    START = "2000-01-01 10:10"
    CASES = [[0,arrow.get("2000-01-01 10:10"), arrow.get("2000-01-01 11:10")],
             [200,arrow.get("2000-01-01 16:03"), arrow.get("2000-01-01 23:30")],
             [500,arrow.get("2000-01-02 01:38"), arrow.get("2000-01-02 19:30")],
             [800,arrow.get("2000-01-02 12:07"), arrow.get("2000-01-03 19:40")],
             [1000,arrow.get("2000-01-02 19:15"), arrow.get("2000-01-04 13:10")]
             ]
    for case in CASES:
        assert open_time(case[0], DIST, START) == case[1].isoformat()
        assert close_time(case[0], DIST, START) == case[2].isoformat()


def test_special():
    assert open_time(200,200,"2000-01-01 10:10") == arrow.get("2000-01-01 16:03").isoformat()
    assert close_time(210,200,"2000-01-01 10:10") == arrow.get("2000-01-01 23:40").isoformat()
    assert close_time(310,300,"2000-01-01 10:10") == arrow.get("2000-01-02 06:10").isoformat()
    assert close_time(410,400,"2000-01-01 10:10") == arrow.get("2000-01-02 13:10").isoformat()
    assert close_time(610,600,"2000-01-01 10:10") == arrow.get("2000-01-03 02:10").isoformat()
    assert close_time(1010,1000,"2000-01-01 10:10") == arrow.get("2000-01-04 13:10").isoformat()
