"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

@app.route("/_calc_times")
def _calc_times():
  """
  Calculates open/close times from miles, using rules 
  described at https://rusa.org/octime_alg.html.
  Expects one URL-encoded argument, the number of miles. 
  """
  app.logger.debug("Got a JSON request");
  km = request.args.get('km', 0, type=float)
  miles = request.args.get('miles', 0, type=float)
  begin_date = request.args.get('begin_date', "begin_date", type=str)
  begin_time = request.args.get('begin_time', "begin_time", type=str)
  distance_km = request.args.get('distance_km', 0 , type=int)
  print(distance_km)
  print(str(km) + " this is km in cal_times")
  #print(begin_date + " this is km in date")
  #print(begin_time + " this is km in time")
  #FIXME: These probably aren't the right open and close times
  start_time = begin_date + " " +  begin_time
  print(miles)
  if km == 0:
      km = (1.609344 * miles)
  open_time = acp_times.open_time(km, distance_km, start_time)
  close_time = acp_times.close_time(km, distance_km, start_time)
  result={ "open": open_time, "close": close_time }
  return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
